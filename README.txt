
Hello :) ..  please find below all of the information regarding my implementation of the task.
-----------------------------------------------------------------------------------------------------------


- Folder Structure
------------------
	- index file
	- assets folder
	    - css folder [all css files]
	    - js folder
	        - libs folder [all used libraries]
		    - angular folder [angular files for the task]
        - fonts folder
	    - partials folder [partials for the task]

- Libraries
-----------
	- Libraries used to implement the task are [JQuery - High Charts - AngularJS].


- JS design pattern
-------------
	- Revealing module pattern is used for code modularization in angularjs task files.


- User Experience (UX)
-----------------------------------
	- The first view is a list of cities boxes with the cities names .
    - when the user hover over the city box .. the sunrise and sunset time is displayed overlayed the city box
    - when the user clicks on the city box, a popup is displayed with chart of the sea level forecast for the next five days for the clicked city
    - when user clicks out side of the popup, the popup disappears.

- Browsers
	- the code perfectly tested on chrome and internet explorer

- General
    - I tried to make the implementation as simple as possible and not to use any third party libraries or plugins except the most necessary ones (angularJS - Highcharts - Jquery)

Note : to run the task properly you have to run index.html in a local server

------------------------------------------------------ Thank You :) -----------------------------------------------------------------








 