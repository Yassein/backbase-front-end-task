(function(){
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    // route config
    app.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when('/', {
                    templateUrl: './assets/partials/weather.html',
                    controller: 'weatherCtrl'
                })
        }]);

})();

