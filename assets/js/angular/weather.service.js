(function(){
    'use strict';

    angular
        .module('app')
        .factory('weatherSrv', weatherSrv);

    weatherSrv.$inject = ['weatherData'];

    function weatherSrv(weatherData) {

        // Public APIs
        return {
            getCitiesInfo: getCitiesInfo,
            getSeaLevelForecast: getSeaLevelForecast
        };


        /**
         * get a list of cities' weather data
         * @param {array} citiesList
         * @returns {array} list of objects with mapped data
         */
        function getCitiesInfo(citiesList) {
            return weatherData.getCitiesInfo(citiesList).then(function(cities) {
                return cities.map(function(city) {
                    return {
                        sunrise: (new Date( city.data.sys.sunrise * 1000)).toLocaleTimeString(),
                        sunset: (new Date( city.data.sys.sunset * 1000)).toLocaleTimeString(),
                        cityName: city.data.name
                    }
                });
            });
        }

        /**
         * get Sea Level Forecast for the upcoming 5 days
         * @param {city} city name
         * @returns {array} list of sea levels values
         */
        function getSeaLevelForecast(city) {
            return weatherData.getSeaLevelForecast(city).then(function (days) {
                return days.data.list.map(function (day) {
                    return day.pressure;
                });
            })
        }
    }

})();
