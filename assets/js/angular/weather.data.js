(function(){
    'use strict';

    angular
        .module('app')
        .factory('weatherData', weatherData);

    weatherData.$inject = ['$http', '$q'];

    /**
     * weather data service - the data layer for comunicating with the api services
     */
    function weatherData($http, $q) {

        // api key
        var appId = "3d8b309701a13f65b660fa2c64cdc517";

        // Public APIs
        return {
            getCitiesInfo: getCitiesInfo,
            getSeaLevelForecast: getSeaLevelForecast
        }; 
        
        /**
         * get Cities Info
         * @param {array} list of cities.
         * @returns {array} list of cities weather data
         */
        function getCitiesInfo(citiesList) {
            var requests = [];
            var url = '';
            var queue;

            citiesList.forEach(function(item, index) {
                url = "http://api.openweathermap.org/data/2.5/weather?q="+ item.city + ',' + item.country + "&appid=" + appId;
                requests.push($http({
                    method: 'get',
                    timeout: 20000,
                    cache: false,
                    url: url
                }))
            });

            // combining all requests with one request using $q service
            queue = $q.all(requests).then(function(response) {
                return response;
            });

            return (queue.then(handleSuccess, handleError));
        }

        /**
         * get Sea level forecast (for five days)
         * @param {string} city name.
         * @returns {array} list of weather info for the next five days for the specified city
         */
        function getSeaLevelForecast(city) {
            var url = "http://api.openweathermap.org/data/2.5/forecast/daily?q="+ city +"&units=metric&cnt=5&appid=" + appId;
            return $http({
                method: 'get',
                timeout: 20000,
                cache: false,
                url: url
            }).then(handleSuccess, handleError)
        }


        // ---
        // PRIVATE METHODS.
        // ---

        function handleError(rejection) {
            return ($q.reject(rejection));
        }

        function handleSuccess(response) {
            return response;
        }

    }

})();
