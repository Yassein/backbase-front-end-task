(function(){
    'use strict';

    angular
        .module('app')
        .controller('weatherCtrl', weatherCtrl);

    weatherCtrl.$inject = ['$scope', 'weatherSrv'];

    function weatherCtrl($scope, weatherSrv) {

        /**
         * list of the cities to get weather data for
         */
        var citiesList = [
            {city: 'London', country: 'uk'},
            {city: 'Cairo', country: 'egypt'},
            {city: 'Oxford', country: 'uk'},
            {city: 'Alexandria', country: 'egypt'},
            {city: 'Glasgow', country: 'uk'},
            {city: 'Aswan', country: 'egypt'}
        ];

        /**
         * list of colors for widgets background-colors
         */
        var colorsList = ['#00c0e2', '#59d999', '#7658f9', '#e9c24e', '#e55679', '#cb6fd7'];

        $scope.nextDaysArr = GetDays(new Date(), 5);
        $scope.colors = colorsList;
        $scope.showBg = false;

        /**
         * map the day index to day name
         * @param {number} day index.
         * @returns {string} day name
         */
        function DayAsString(dayIndex) {
            var weekdays = new Array(7);
            weekdays[0] = "Sunday";
            weekdays[1] = "Monday";
            weekdays[2] = "Tuesday";
            weekdays[3] = "Wednesday";
            weekdays[4] = "Thursday";
            weekdays[5] = "Friday";
            weekdays[6] = "Saturday";

            return weekdays[dayIndex];
        }

        /**
         * get a list of the upcoming dates
         * @param {date} start date.
         * @param {number} days to add
         * @returns {array} list of days names
         */
        function GetDays(startDate, daysToAdd) {
            var aryDates = [];

            for(var i = 0; i <= daysToAdd; i++) {
                var currentDate = new Date();
                currentDate.setDate(startDate.getDate() + i);
                aryDates.push(DayAsString(currentDate.getDay()));
            }

            return aryDates;
        }

        // call get cities info service and bind response to scope object
        weatherSrv.getCitiesInfo(citiesList).then(function(cities) {
            $scope.cities = cities;
        }).catch(function (error) {
            console.log(error);
        });

        /**
         * on widget click handler
         * @param {city} city name.
         */
        $scope.onWidgetClick = function (city) {

            $scope.showBg = true;

            // get sea level forecast
            weatherSrv.getSeaLevelForecast(city).then(function (response) {

                // draw the chart with response data
                Highcharts.chart('chartContainer', {
                    title: {
                        text: 'Sea Level Forecast for the next five days for ' + city
                    },
                    xAxis: {
                       categories: $scope.nextDaysArr,
                        title: {
                            text: 'Next Days'
                        },
                    },
                    yAxis: {
                        title: {
                            text: 'Sea Level'
                        },
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br />',
                        pointFormat: 'x = {point.x}, y = {point.y}'
                    },
                    series: [{
                        data: response,
                        pointStart: 1
                    }]
                });
            }).catch(function (error) {
                console.log(error);
            })
        }


    }

})();
