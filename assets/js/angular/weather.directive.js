(function(){
    'use strict';

    angular
        .module('app')
        .directive('weatherWidget', weatherWidget);

    /**
     * a reusable widget directive 
     */
    function weatherWidget() {

        return {
            restrict: 'E',
            scope: {
                city: '@',
                sunrise: '@',
                sunset: '@',
                color: '@',
                onClick: '&'
            },
            templateUrl: 'assets/partials/weather-widget.html',
            link: function ($scope, element, attrs) {
              element.click(function () {
                  $scope.onClick();
              });
            }
        }
    }

})();